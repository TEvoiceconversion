#! praat
# 


########################################################################
# 
# TestVoiceConversion.praat
#
# Run a test over VoiceConversion, changing individual parameters one at
# a time.
#
########################################################################
#
# Copyright (C) 2017 NKI-AVL, R. J. J. H. van Son
# R.v.Son@nki.nl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Full license text is available at:
# http://www.gnu.org/licenses/gpl-3.0.html
#
########################################################################
#
createDirectory: "Test"

# Pitch
appendInfoLine: "Pitch"
runScript: "VoiceConversion.praat", "aa.wav", 70, 0, 0,	0, 0, 0, 0, 0, 15, 0
Save as WAV file: "Test/aa_Pitch_70.wav"
select all
Remove

runScript: "VoiceConversion.praat", "ookhetweer.wav", 70, 0, 0,	0, 0, 0, 0, 0, 15, 0
Save as WAV file: "Test/ookhetweer_Pitch_70.wav"
select all
Remove

# Pitch_SD
appendInfoLine: "Pitch_SD"
runScript: "VoiceConversion.praat", "ookhetweer.wav", 70, 10, 0,	0, 0, 0, 0, 0, 15, 0
Save as WAV file: "Test/ookhetweer_Pitch_70_SD_10.wav"
select all
Remove

# Duration
appendInfoLine: "Duration"
runScript: "VoiceConversion.praat", "ookhetweer.wav", 0, 0, 1.3, 0, 0, 0, 0, 0, 15, 0
Save as WAV file: "Test/ookhetweer_Duration_1.3.wav"
select all
Remove

# HNR
appendInfoLine: "HNR"
runScript: "VoiceConversion.praat", "aa.wav", 0, 0, 0, 5, 0, 0, 0, 0, 15, 0
Save as WAV file: "Test/aa_HNR_5.wav"
select all
Remove

appendInfoLine: "HNR"
runScript: "VoiceConversion.praat", "ookhetweer.wav", 0, 0, 0, 5, 0, 0, 0, 0, 15, 0
Save as WAV file: "Test/ookhetweer_HNR_5.wav"
select all
Remove

# Bubbles, Bubbles_SNR
appendInfoLine: "Bubbles_SNR"
runScript: "VoiceConversion.praat", "aa.wav", 0, 0, 0, 0, 1, -10, 0, 0, 15, 0
Save as WAV file: "Test/aa_Bubbles_1_-5.wav"
select all
Remove

runScript: "VoiceConversion.praat", "ookhetweer.wav", 0, 0, 0, 0, 1, -10, 0, 0, 15, 0
Save as WAV file: "Test/ookhetweer_Bubbles_1_-5.wav"
select all
Remove

# Jitter
appendInfoLine: "Jitter"
runScript: "VoiceConversion.praat", "aa.wav", 0, 0, 0, 0, 0, 0, 5, 0, 15, 0
Save as WAV file: "Test/aa_Jitter_5.wav"
select all
Remove

# Shimmer
appendInfoLine: "Shimmer"
runScript: "VoiceConversion.praat", "aa.wav", 0, 0, 0, 0, 0, 0, 0, 10, 15, 0
Save as WAV file: "Test/aa_Shimmer_10.wav"
select all
Remove

